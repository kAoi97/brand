# Changelog
All notable changes to this repository will be documented in this file.

## [0.7] - 2020-05-04
### Added
- Prototype version of future brouchures.
### Changed
- Finished the spanish version of bad applications.

## [0.6] - 2020-03-15
### Added
- Missing versions of imagotype.
### Changed
- Folders order.
- Name of some files and folders.

## [0.5] - 2019-09-01
### Added
- Iconology folder with some useful icons.
### Fixed
- Brand color chart in brand manuals.
### Changed
- Availability of formats in profile icons (social Media folder).

## [0.4] - 2019-04-29
### Added
- some progress for the manual in the Spanish version (finalized 1st section).

## [0.3] - 2019-04-20
### Added
- File _LICENCE_
### Fixed
- Change of extension to this file _CHANGELOG_ by _CHANGELOG.md_
- Edit minor fixes, and change the name of Readme.md to README.md

## [0.2] - 2019-04-08
### Added
- File _README.md_ and _CHANGELOG_ with the clarifications needed to make an effective and consistent use of this repository.

### Changed
- Relevant information for the _README.md_ file.

## [0.1] - 2019-03-31
### Added
- Some basic files prepared previously regarding the brand image.
- _.gitignore_ file with the exceptions required by this repository.
