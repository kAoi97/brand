<img width="200" src="https://gitlab.com/kAoi97/brand/raw/master/Imagotype/bitmap/horisontal/kAoi97%20horisontal%20imagotype.png" /><hr>
 
 # Brand #

Below are the guidelines, rules, recommendations, elements and supplies; as well source code, 
files and documents necessary to make a correct and effective application of the kAoi97 Brand.

Don't forget to visit the _About kAoi97_ and _Use of Brand_ sections in my website to better understand where 
the graphic concept comes from and what each element means or indicates.

*  [About kAoi97](https://www.kaoi97.net/about)
*  [Use of Brand](https://www.kaoi97.net/brand)

## Table of Contents

1. [Usage](#usage)
2. [Clone / Download the Repository](#clone-download-the-repository)
3. [Changelog](#changelog)
4. [Support](#support)
5. [License](#license)

## In this repository
Below you will find a table of resources available in this repository

| Folder       | description                                                 |
| ------------ | ----------------------------------------------------------- |
| Applications | Specific applications of the brand, such as business cards  |
| Brand Manual | English, Español and 日本語 version of Brand Manual         |
| Imagotype    | Different versions of the imagotype, (include source files) |


## Usage
This repository is intended to house all the necessary resources and guidelines to make the correct 
use of the colors, fonts, designs, logos, isotypes, symbols, and other elements of the kAoi97 brand, 
positioning them in the different communicative applications necessary to strengthen the commitment 
and the sense of belonging inherent in the brand.

To that extent, those people who make use of elements, works and references identified with the kAoi97 
Brand should be empowered of all the resources available in this repository and develop in an accurate 
and unequivocal way the brand image `without no exception`.

### Steps to use this repository

1.   First of all, check the [Brand Manual](https://gitlab.com/kAoi97/brand/tree/master/Brand%20Manual) 
and the different possibilities of graphic application in the described one.  

2.   Then define that you need to use the brand; only the logo in horizontal version in bitmap, or require the entire 
resource package including the vector source file, and based on your decision define whether to 
[clone or download](#clone-download-the-repository) the entire repository or only what you need.  

3.   Finally, check that all graphic elements used comply with the guidelines established in the _Brand Manual_ and have 
a harmonious relationship with the surrounding elements.

Don't forget that if you have doubts about how to use or apply any of the elements of the brand you can write me to the 
contact information available in [Support](#support).


## Clone / Download the Repository

You can choose any of the options listed below.

#### Clone the Repository

If you think that you will need several and / or recurrently the resources of the brand, you also have a basic management 
of git you can clone my repository and be up to date with the latest changes and additions to the repository later; simply 
have installed [git](https://git-scm.com/) on your computer, open a terminal in the folder where you want to store the 
repository and execute the following command.

```
git clone https://gitlab.com/kAoi97/brand.git

```
Remember to avoid editing the repository files directly, instead you can make a copy out of the repository folder and edit 
it with confidence to avoid conflicts when you want to refresh (pull request) future possible changes.

#### Download

If you do not have git handling, you can simply download a compressed copy of the repository that includes all the files; 
or failing that, navigate through the different repository directories and access the specific resource required.

*  [Download .zip](https://gitlab.com/kAoi97/brand/-/archive/master/brand-master.zip)
*  [Download .tar.gz](https://gitlab.com/kAoi97/brand/-/archive/master/brand-master.tar.gz)

## Changelog

If you have cloned this repository before or have a downloaded copy, do not forget to check the [changelog](CHANGELOG.md) to make sure you 
are working with the _latest changes uploaded_ to the repository

## Support

If you have any doubts or suggestions when using the elements provided in this repository, do not hesitate to contact me 
through the different means and networks available below, I will be attentive to answer any questions in the shortest 
possible time

[www.kaoi97.net](https://www.kaoi97.net)  

[leonardo@kaoi97.net](mailto:leonardo@kaoi97.net)  

[(+57)314 4818811](https://t.me/kAoi97)  


## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
<br />
<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">kAoi97 Brand</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.kaoi97.net/LeoElBaku" property="cc:attributionName" rel="cc:attributionURL">Leonardo Alvarado Guio</a>
is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />
Permissions beyond the scope of this license may be available at <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.kaoi97.net/terms-and-conditions" rel="cc:morePermissions">https://www.kaoi97.net/terms-and-conditions</a>.
  
